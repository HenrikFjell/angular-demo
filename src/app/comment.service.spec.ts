import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CommentService, Comment } from './comment.service';

describe('CommentService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
      HttpClientTestingModule
    ]
  }));

  it('should be created', () => {
    const service: CommentService = TestBed.get(CommentService);
    expect(service).toBeTruthy();
  });

  it(`should issue a request`, async () => {
    const backend = TestBed.get(HttpTestingController)
    const service: CommentService = TestBed.get(CommentService);

    service.getComments().subscribe()

    backend.expectOne({
      method: 'GET'
    })
  })

  it(`should get some data`, async () => {
    const data: Comment[] = [
      {
        commentText: 'I fully agree',
        commentDate: '2019-07-28T18:37:00'
      },
      {
        commentText: 'I prefer reactive',
        commentDate: '2019-07-28T18:42:00'
      }
    ]

    const service: CommentService = TestBed.get(CommentService)
    const backend:HttpTestingController = TestBed.get(HttpTestingController)

    service.getComments().subscribe((next) => {
      expect(next).toEqual(data)
    })

    const request = backend.match({method: 'GET'})
    request[0].flush(data)
  })

})
