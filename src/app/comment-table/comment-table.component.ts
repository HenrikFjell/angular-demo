import { Component, OnInit } from '@angular/core';
import { CommentService, Comment } from '../comment.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-comment-table',
  templateUrl: './comment-table.component.html',
  styleUrls: ['./comment-table.component.scss']
})
export class CommentTableComponent implements OnInit {
  displayedColumns: string[] = ['commentText', 'commentDate']
  comments: Observable<Comment[]>

  constructor(private commentService: CommentService) { }

  ngOnInit() {
    this.comments = this.commentService.getComments()
  }
}
