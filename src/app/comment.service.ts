import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

export interface Comment {
  commentText: string
  commentDate: string
}

@Injectable({
  providedIn: 'root'
})

export class CommentService {

  readonly commentsUrl = 'assets/comments.json'

  constructor(private http: HttpClient) { }

  getComments() {
    return this.http.get<Comment[]>(this.commentsUrl)
  }
}
